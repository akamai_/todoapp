//
//  EmptyListView.swift
//  ToDoAPP
//
//  Created by Robert Szost on 18/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import UIKit

class EmptyListView: UIView {
    
    lazy var backgroundView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.image = #imageLiteral(resourceName: "emptyBackground")
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var tasksNotFoundImage: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "cannotFind"))
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Your task list is empty. "
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.7957606403)
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Why don't you add one more?"
        label.textColor = #colorLiteral(red: 0.4717634916, green: 0.4718467593, blue: 0.4717524648, alpha: 0.1967503834)
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var addNewTaskButton: UIButton = {
        let button = UIButton()
        button.setTitle("ADD NEW", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 12)
        button.titleLabel?.textColor = .white
        button.backgroundColor = #colorLiteral(red: 0.9872989058, green: 0.7894297242, blue: 0.3103260398, alpha: 1)
        button.contentMode = .center
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 2
        button.layer.borderColor = #colorLiteral(red: 0.9598398805, green: 0.816591084, blue: 0.5767042637, alpha: 1)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        createHierarchy()
        setupConstraints()
    }
    
    private func createHierarchy() {
        addSubview(backgroundView)
        backgroundView.addSubview(tasksNotFoundImage)
        backgroundView.addSubview(titleLabel)
        backgroundView.addSubview(subtitleLabel)
        backgroundView.addSubview(addNewTaskButton)

    }
    
    private func setupConstraints() {
        setupConstraintsBackgroundView()
        setupConstraintsTasksNotFoundImage()
        setupConstraintsTitleLabel()
        setupConstraintsSubtitleLabel()
        setupConstraintsAddNewTaskButton()
    }
    
    
    private func setupConstraintsBackgroundView() {
        NSLayoutConstraint.activate([
            backgroundView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            backgroundView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
            backgroundView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            backgroundView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
        ])
    }
    
    private func setupConstraintsTasksNotFoundImage() {
        NSLayoutConstraint.activate([
            tasksNotFoundImage.centerXAnchor.constraint(equalTo: backgroundView.centerXAnchor),
            tasksNotFoundImage.centerYAnchor.constraint(equalTo: backgroundView.centerYAnchor, constant: frame.height * -0.05),
        ])
    }

    
    private func setupConstraintsTitleLabel() {
        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: backgroundView.centerXAnchor),
            titleLabel.topAnchor.constraint(equalTo: tasksNotFoundImage.bottomAnchor, constant: frame.height * 0.03)
        ])
    }
    private func setupConstraintsSubtitleLabel() {
        NSLayoutConstraint.activate([
            subtitleLabel.centerXAnchor.constraint(equalTo: backgroundView.centerXAnchor),
            subtitleLabel.bottomAnchor.constraint(equalTo: addNewTaskButton.topAnchor, constant: -10)
        ])
    }
    
    private func setupConstraintsAddNewTaskButton() {
        NSLayoutConstraint.activate([
            addNewTaskButton.centerXAnchor.constraint(equalTo: backgroundView.centerXAnchor),
            addNewTaskButton.widthAnchor.constraint(equalToConstant: frame.width * 0.4),
            addNewTaskButton.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: frame.height * -0.05)
        ])
    }
}
