//
//  AddNewTaskView.swift
//  ToDoAPP
//
//  Created by Robert Szost on 19/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import UIKit

class AddNewTaskView: UIView {
    
    let addNewTaskViewModel: AddNewTaskViewModel
    
    let taskTitleTextField: UITextField = {
        let textField = UITextField()
        textField.tag = 1001
        textField.backgroundColor = #colorLiteral(red: 0.9489200711, green: 0.9490788579, blue: 0.9488992095, alpha: 1)
        textField.placeholder = "Title of the task"
        textField.layer.cornerRadius = 10
        textField.textAlignment = .center
        textField.layer.borderColor = #colorLiteral(red: 0.9327258468, green: 0.6330868006, blue: 0.3118772507, alpha: 1)
        textField.layer.borderWidth = 1
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    let taskDescriptionTextField: UITextField = {
        let textField = UITextField()
        textField.tag = 1002
        textField.backgroundColor = #colorLiteral(red: 0.9489200711, green: 0.9490788579, blue: 0.9488992095, alpha: 1)
        textField.placeholder = "Description of the task"
        textField.layer.cornerRadius = 10
        textField.textAlignment = .center
        textField.layer.borderColor = #colorLiteral(red: 0.9327258468, green: 0.6330868006, blue: 0.3118772507, alpha: 1)
        textField.layer.borderWidth = 1
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    let categoryPicker: UIPickerView = {
        let picker = UIPickerView()

        picker.translatesAutoresizingMaskIntoConstraints = false
        return picker
    }()
    
    lazy var createTaskButton: UIButton = {
        let button = UIButton()
        button.setTitle("CREATE TASK", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 12)
        button.titleLabel?.textColor = .white
        button.backgroundColor = #colorLiteral(red: 0.9872989058, green: 0.7894297242, blue: 0.3103260398, alpha: 1)
        button.contentMode = .center
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 2
        button.layer.borderColor = #colorLiteral(red: 0.9598398805, green: 0.816591084, blue: 0.5767042637, alpha: 1)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.setTitle("CANCEL", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 12)
        button.titleLabel?.textColor = .white
        button.backgroundColor = #colorLiteral(red: 0.9489166141, green: 0.9490789771, blue: 0.9489063621, alpha: 1)
        button.contentMode = .center
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 2
        button.layer.borderColor = #colorLiteral(red: 0.9598398805, green: 0.816591084, blue: 0.5767042637, alpha: 1)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    init(frame: CGRect = .zero, addNewTaskViewModel: AddNewTaskViewModel) {
        self.addNewTaskViewModel = addNewTaskViewModel
        self.addNewTaskViewModel.newTask = Task(title: "", description: "", category: .other)
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        self.backgroundColor = .white
        setupDelegates()
        createHierarchy()
        bindView()
        setupConstraints()
    }
    
    private func setupDelegates() {
        categoryPicker.delegate = addNewTaskViewModel
        categoryPicker.dataSource = addNewTaskViewModel
        taskTitleTextField.delegate = addNewTaskViewModel
        taskDescriptionTextField.delegate = addNewTaskViewModel
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    private func createHierarchy() {
        addSubview(taskTitleTextField)
        addSubview(taskDescriptionTextField)
        addSubview(categoryPicker)
        addSubview(createTaskButton)
        addSubview(cancelButton)
    }
    
    private func setupConstraints() {
        setupConstraintsTaskTitleTextField()
        setupConstraintsTaskDescriptionTextField()
        setupConstraintsCategoryPicker()
        setupConstraintsCreateTaskButton()
        setupConstraintsCancelButton()
    }
    
    private func bindView() {
        createTaskButton.addTarget(addNewTaskViewModel, action: #selector(addNewTaskViewModel.saveButtonTapped), for: .touchUpInside)
        cancelButton.addTarget(addNewTaskViewModel, action: #selector(addNewTaskViewModel.cancelButtonTapped), for: .touchUpInside)
    }
    
    private func setupConstraintsTaskTitleTextField() {
        NSLayoutConstraint.activate([
        taskTitleTextField.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: frame.height * 0.1),
            taskTitleTextField.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
            taskTitleTextField.widthAnchor.constraint(equalToConstant: frame.width * 0.7),
            taskTitleTextField.heightAnchor.constraint(equalToConstant: frame.height * 0.05)
        ])
    }
    
    private func setupConstraintsTaskDescriptionTextField() {
        NSLayoutConstraint.activate([
        taskDescriptionTextField.topAnchor.constraint(equalTo: taskTitleTextField.bottomAnchor, constant: frame.height * 0.05),
            taskDescriptionTextField.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
                        taskDescriptionTextField.widthAnchor.constraint(equalToConstant: frame.width * 0.7),
            taskDescriptionTextField.heightAnchor.constraint(equalToConstant: frame.height * 0.1)
        ])
    }
    
    private func setupConstraintsCategoryPicker() {
        NSLayoutConstraint.activate([
            categoryPicker.topAnchor.constraint(equalTo: taskDescriptionTextField.bottomAnchor, constant: frame.height * 0.05),
            categoryPicker.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
            categoryPicker.heightAnchor.constraint(equalToConstant: frame.height * 0.15)
        ])
    }
    
    private func setupConstraintsCreateTaskButton() {
        NSLayoutConstraint.activate([
            createTaskButton.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: frame.width * 0.1),
            createTaskButton.widthAnchor.constraint(equalToConstant: frame.width * 0.4),
            createTaskButton.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: frame.height * -0.05)
        ])
    }
    
    private func setupConstraintsCancelButton() {
        NSLayoutConstraint.activate([
            cancelButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: frame.width * -0.1),
            cancelButton.widthAnchor.constraint(equalToConstant: frame.width * 0.4),
            cancelButton.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: frame.height * -0.05)
        ])
    }
}
