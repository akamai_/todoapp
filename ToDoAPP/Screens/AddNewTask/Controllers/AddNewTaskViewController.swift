//
//  AddNewTaskViewController.swift
//  ToDoAPP
//
//  Created by Robert Szost on 19/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import UIKit

class AddNewTaskViewController: UIViewController {
    var addNewTaskViewModel: AddNewTaskViewModel!
    
    init(addNewTaskViewModel: AddNewTaskViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.addNewTaskViewModel = addNewTaskViewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
        
    public override  func loadView() {
        view = AddNewTaskView(addNewTaskViewModel: addNewTaskViewModel)
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        bindNavigation()
        hideKeyboardWhenTappedAround()
    }
        
    func bindNavigation() {
        addNewTaskViewModel.saveAndDismissViewController = { [weak self] in
            guard let self = self else { return }
            let taskToSave = self.addNewTaskViewModel.newTask
            DataManager.shared.addNewTask(task: taskToSave!)
            self.alertManager(isSuccesful: true)
        }
        
        addNewTaskViewModel.cancelAndDismissViewController = { [weak self] in
            guard let self = self else { return }
            self.alertManager(isSuccesful: false)

        }
    }
}
