//
//  AddNewTaskViewModel.swift
//  ToDoAPP
//
//  Created by Robert Szost on 19/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import UIKit

class AddNewTaskViewModel: NSObject {
    var newTask: Task?
    var categories = ["Work", "Shopping", "Other"]
    var saveAndDismissViewController: (() -> Void)?
    var cancelAndDismissViewController: (() -> Void)?
    
    @objc func saveButtonTapped() {
        saveAndDismissViewController!()
    }
    
    @objc func cancelButtonTapped() {
        cancelAndDismissViewController!()
    }
}

extension AddNewTaskViewModel: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
            case 1001:
                newTask?.title = textField.text!
            case 1002:
                newTask?.description = textField.text!
            default:
                print("Something really wrong happend!")
            }
    }
}

extension AddNewTaskViewModel: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
       return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       return categories.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row == 0 {
            newTask?.category =  .work
        }else if row == 1 {
            newTask?.category = .shopping
        }else {
            newTask?.category = .other
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       let row = categories[row]
       return row
    }

    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleData = categories[row]
        let myTitle = NSAttributedString(string: titleData, attributes: [.foregroundColor:UIColor(cgColor: #colorLiteral(red: 0.9426779151, green: 0.7898855805, blue: 0.6309406757, alpha: 1))])
        return myTitle
    }
}
