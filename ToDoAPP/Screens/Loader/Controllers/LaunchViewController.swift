//
//  LaunchViewController.swift
//  ToDoAPP
//
//  Created by Robert Szost on 17/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {
    
    lazy var logoImage: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "logo"))
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var sampleLabel: UILabel = {
        let label = UILabel()
        label.text = "ToDoApp"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        createHierarchy()
        setupConstraints()
    }
    
    private func createHierarchy() {
        view.addSubview(logoImage)
        view.addSubview(sampleLabel)
    }
    
    private func setupConstraints() {
        logoImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        logoImage.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        logoImage.widthAnchor.constraint(equalToConstant: 50).isActive = true
        sampleLabel.topAnchor.constraint(equalTo: logoImage.bottomAnchor).isActive = true
        sampleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
}
