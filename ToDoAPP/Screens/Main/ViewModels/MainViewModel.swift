//
//  MainViewModel.swift
//  ToDoAPP
//
//  Created by Robert Szost on 17/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import UIKit

class MainViewModel: NSObject {
    var createdTasks: [Task] = []
    var presentAddNewTaskViewController: (() -> Void)?
    
    @objc func addNewTaskButtonTapped(){
        presentAddNewTaskViewController!()
    }
}

extension MainViewModel: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return createdTasks.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell", for: indexPath) as! TaskCell
        cell.configureCell(with: createdTasks[indexPath.row].title, description: createdTasks[indexPath.row].description, category: createdTasks[indexPath.row].category)
        cell.contentView.layer.cornerRadius = 20
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        createdTasks.append(Task(title: "title", description: "description", category: .other)
//        DataManager.shared.tasks = createdTasks
//        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            createdTasks.remove(at: indexPath.row)
            DataManager.shared.tasks = createdTasks
            DataManager.shared.saveTasksToFile()
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

}
