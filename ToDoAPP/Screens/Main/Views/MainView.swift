//
//  MainView.swift
//  ToDoAPP
//
//  Created by Robert Szost on 17/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import UIKit

class MainView: UIView {
    
    let mainViewModel: MainViewModel

    lazy var tableView: UITableView = {
        let tb = UITableView()
        tb.backgroundColor = .clear
        tb.separatorStyle = .none
        tb.isScrollEnabled = true
        tb.register(TaskCell.self, forCellReuseIdentifier: "TaskCell")
        tb.translatesAutoresizingMaskIntoConstraints = false
        return tb
    }()
    
    lazy var addNewTaskButton: UIButton = {
        let button = UIButton()
        button.setTitle("ADD NEW", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 12)
        button.titleLabel?.textColor = .white
        button.backgroundColor = #colorLiteral(red: 0.9872989058, green: 0.7894297242, blue: 0.3103260398, alpha: 1)
        button.contentMode = .center
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 2
        button.layer.borderColor = #colorLiteral(red: 0.9598398805, green: 0.816591084, blue: 0.5767042637, alpha: 1)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    init(frame: CGRect = .zero, viewModel: MainViewModel) {
        self.mainViewModel = viewModel
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        self.backgroundColor = .white
        tableView.delegate = mainViewModel
        tableView.dataSource = mainViewModel
        constructHierarchy()
        bindView()
        activateConstraints()
    }
    
    func constructHierarchy(){
        addSubview(tableView)
        addSubview(addNewTaskButton)
    }
    
    func activateConstraints() {
        activateConstraintsTableView()
        activateConstraintsAddNewTaskButton()
    }
    
    func bindView() {
        addNewTaskButton.addTarget(mainViewModel, action: #selector(mainViewModel.addNewTaskButtonTapped), for: .touchUpInside)
    }
    
    func activateConstraintsTableView() {
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: frame.height * 0.05),
            tableView.bottomAnchor.constraint(equalTo: addNewTaskButton.topAnchor, constant: frame.height * -0.01),
            tableView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor)])
    }
    
    func activateConstraintsAddNewTaskButton() {
        NSLayoutConstraint.activate([
            addNewTaskButton.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
            addNewTaskButton.widthAnchor.constraint(equalToConstant: frame.width * 0.4),
            addNewTaskButton.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: frame.height * -0.05)
        ])
    }
}
