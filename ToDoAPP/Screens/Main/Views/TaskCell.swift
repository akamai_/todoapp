//
//  TaskCell.swift
//  ToDoAPP
//
//  Created by Robert Szost on 17/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import UIKit
    
class TaskCell: UITableViewCell {
    
    lazy var titleOfTaskCell: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont(name: "HelveticaNeue-Medium", size: 20.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var descriptionOfTaskCell: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .left
        label.font = UIFont(name: "HelveticaNeue-Regular", size: 16.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var categoryIconOfTaskCell: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .white
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 20
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var backgroundOfTaskCell: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(cgColor: #colorLiteral(red: 0.9530361295, green: 0.798951745, blue: 0.5534208417, alpha: 1))
        view.layer.cornerRadius = 10
        view.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .clear
        constructHierarchy()
        activateConstraints()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureCell(with title: String, description: String, category: CategoriesOfTasks) {
        titleOfTaskCell.text = title
        descriptionOfTaskCell.text = description
        categoryIconOfTaskCell.image = category.image
    }
    
    func constructHierarchy() {
        self.addSubview(backgroundOfTaskCell)
        backgroundOfTaskCell.addSubview(titleOfTaskCell)
        backgroundOfTaskCell.addSubview(descriptionOfTaskCell)
        backgroundOfTaskCell.addSubview(categoryIconOfTaskCell)
    }
    
    func activateConstraints() {
        activateConstraintsBackgroundOfTaskCell()
        activateConstraintsTitleOfTaskCell()
        activateConstraintsDescriptionOfTaskCell()
        activateConstraintsCategoryIconOfTaskCell()
    }
    
    func activateConstraintsBackgroundOfTaskCell() {
        NSLayoutConstraint.activate([
            backgroundOfTaskCell.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            backgroundOfTaskCell.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: frame.width * -0.05),
            backgroundOfTaskCell.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            backgroundOfTaskCell.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
        ])
    }
    
    func activateConstraintsTitleOfTaskCell() {
        NSLayoutConstraint.activate([
            titleOfTaskCell.centerXAnchor.constraint(equalTo: backgroundOfTaskCell.centerXAnchor),
            titleOfTaskCell.topAnchor.constraint(equalTo: backgroundOfTaskCell.topAnchor, constant: 10),
        ])
    }
    
    func activateConstraintsDescriptionOfTaskCell() {
        NSLayoutConstraint.activate([
            descriptionOfTaskCell.topAnchor.constraint(equalTo: titleOfTaskCell.bottomAnchor),
            descriptionOfTaskCell.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor),
            descriptionOfTaskCell.trailingAnchor.constraint(equalTo: categoryIconOfTaskCell.leadingAnchor),
        ])
    }
    
    func activateConstraintsCategoryIconOfTaskCell() {
        NSLayoutConstraint.activate([
            categoryIconOfTaskCell.centerYAnchor.constraint(equalTo: descriptionOfTaskCell.centerYAnchor),
            categoryIconOfTaskCell.widthAnchor.constraint(equalToConstant: 32),
            categoryIconOfTaskCell.heightAnchor.constraint(equalToConstant: 32),
            categoryIconOfTaskCell.trailingAnchor.constraint(equalTo: backgroundOfTaskCell.trailingAnchor, constant: frame.width * -0.03),
            categoryIconOfTaskCell.bottomAnchor.constraint(equalTo: backgroundOfTaskCell.bottomAnchor, constant: frame.width * -0.03),

        ])
    }
}
