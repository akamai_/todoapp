//
//  MainViewController.swift
//  ToDoAPP
//
//  Created by Robert Szost on 18/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    var mainViewModel: MainViewModel!

    init(mainViewModel: MainViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.mainViewModel = mainViewModel
        self.mainViewModel.createdTasks = DataManager.shared.loadTasksFromFile()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override  func loadView() {
        if mainViewModel.createdTasks.count == 0 {
            view = EmptyListView()
        }else {
            view = MainView(viewModel: mainViewModel)
        }
    }
    
    override func viewDidLoad() {
        mainViewModel.createdTasks = DataManager.shared.loadTasksFromFile()
        super.viewDidLoad()
        bindNavigation()
    }
    
    func bindNavigation() {
        mainViewModel.presentAddNewTaskViewController = {  [weak self] in
            guard let self = self else { return }
            let addNewTaskViewController = AddNewTaskViewController(addNewTaskViewModel: AddNewTaskViewModel())
            addNewTaskViewController.modalPresentationStyle = .fullScreen
            self.navigationController?.present(addNewTaskViewController, animated: true, completion: nil)
        }
    }
}
