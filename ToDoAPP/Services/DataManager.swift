//
//  DataManager.swift
//  ToDoAPP
//
//  Created by Robert Szost on 18/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import Foundation

final class DataManager {
    
    var tasks : [Task]?
    static let shared = DataManager()
    private init(){}
    
    
    //Finding directory to save
    func documentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }

    //Defining path to save output
    func dataFilePath() -> URL {
        return documentsDirectory().appendingPathComponent("ToDoAPP.plist")
    }

    //Saving all notes from RAM to storage
    func saveTasksToFile() {
        let encoder = PropertyListEncoder()
        
        do {
            let data = try encoder.encode(tasks)
            try data.write(to: dataFilePath(), options: Data.WritingOptions.atomic)
        }catch {
            print("Error encoding item array: \(error.localizedDescription)")
        }
    }
    
    func addNewTask(task: Task) {
        tasks?.append(task)
        saveTasksToFile()
    }

    //Receiving data from storage
    func loadTasksFromFile() -> [Task]{
        let path = dataFilePath()
        print(path)
        if let data = try? Data(contentsOf: path) {
            let decoder = PropertyListDecoder()
            
            do {
                tasks = try decoder.decode([Task].self, from: data)
                return tasks!
            } catch {
                print("Error encoding item array: \(error.localizedDescription)")
            }
        }
        return []
    }
}
