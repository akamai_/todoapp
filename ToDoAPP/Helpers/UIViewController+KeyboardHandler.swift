//
//  UIViewController+KeyboardHandler.swift
//  ToDoAPP
//
//  Created by Robert Szost on 19/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self,action: #selector(hideKeyboard))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
}
