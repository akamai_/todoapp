//
//  AlertManager.swift
//  ToDoAPP
//
//  Created by Robert Szost on 19/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import UIKit

extension UIViewController {
       func alertManager(isSuccesful: Bool){
           if isSuccesful == true {
               let alert = UIAlertController(
                   title: "Saved!",
                   message: "Your task has been saved!",
                   preferredStyle: .alert)
        
               let okAction = UIAlertAction(title: "OK", style: .default, handler: {_ in
                   self.presentingViewController?.dismiss(animated: true, completion: nil)
               })
               present(alert, animated: true, completion: nil)
               alert.addAction(okAction)
           }else {
               let alert = UIAlertController(
                   title: "Changes are not saved",
                   message: "If you continue your task will be not saved.",
                   preferredStyle: .alert)
    
               let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
               let continueAction = UIAlertAction(title: "Continue", style: .destructive, handler: {_ in
                   self.presentingViewController?.dismiss(animated: true, completion: nil)
               })
               present(alert, animated: true, completion: nil)
               alert.addAction(cancelAction)
               alert.addAction(continueAction)
           }
       }
}
