//
//  Task.swift
//  ToDoAPP
//
//  Created by Robert Szost on 17/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import UIKit

enum CategoriesOfTasks: Int, Codable {
    case work
    case shopping
    case other
    
    var image: UIImage {
        switch self {
            case .work:
                return UIImage(#imageLiteral(resourceName: "workIconTaskCategory"))
            case .shopping:
                 return UIImage(#imageLiteral(resourceName: "shoppingIconTaskCategory"))
            case .other:
                 return UIImage(#imageLiteral(resourceName: "otherIconTaskCategory"))
        }
    }
}

struct Task: Codable {
    var title: String
    var description: String
    var category: CategoriesOfTasks
    
    init(title: String, description: String, category: CategoriesOfTasks) {
        self.title = title
        self.description = description
        self.category = category
    }
}
